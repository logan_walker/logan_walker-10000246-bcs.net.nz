﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace gui_Assessment2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public int toChoice = 0;
        public int priorityChoice = 0;
        public string fromAddress = "";
        public string messageText = "";

        public MainPage()
        {
            this.InitializeComponent();
        }

        static string SubjectChoice(int priorityChoice)
        {
            var Choice2 = "";

            switch (priorityChoice)
            {
                case 0:
                    Choice2 = "Low Priority";
                    break;
                case 1:
                    Choice2 = "High Priority";
                    break;
            }
            return Choice2;
        }

        static string ToChoice(int toChoice)
        {
            var Choice3 = "";

            switch (toChoice)
            {
                case 0:
                    Choice3 = "Accounting";
                    break;
                case 1:
                    Choice3 = "Development";
                    break;
                case 2:
                    Choice3 = "HR Management";
                    break;
                case 3:
                    Choice3 = "Marketing";
                    break;
                case 4:
                    Choice3 = "Production";
                    break;
                case 5:
                    Choice3 = "Purchasing";
                    break;
                case 6:
                    Choice3 = "Research";
                    break;

            }
            return Choice3;
        }

        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            fromAddress = FromTextBox.Text;
            messageText = MessageTextBox.Text;
            toChoice = ToComboBox.SelectedIndex;
            priorityChoice = SubjectComboBox.SelectedIndex;

            MessageDialog CheckMessage = new MessageDialog($"To: {ToChoice(toChoice)}\nFrom: {fromAddress}\nSubject: {SubjectChoice(priorityChoice)}\nYour message:\n{messageText}");
            CheckMessage.Title = "Message Preview";

            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Send") { Id = 0 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Reset") { Id = 2 });

            CheckMessage.DefaultCommandIndex = 0;
            CheckMessage.CancelCommandIndex = 1;

            var result = await CheckMessage.ShowAsync();

            if ((int)result.Id == 0)
            {
                Application.Current.Exit();
            }
            else if ((int)result.Id == 2)
            {
                ToComboBox.SelectedIndex = -1;
                FromTextBox.Text = String.Empty;
                SubjectComboBox.SelectedIndex = -1;
                MessageTextBox.Text = string.Empty;
            }
        }
    }
}
